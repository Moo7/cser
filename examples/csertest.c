/** 
 * Serial communication test application.  
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cser.h>

#ifdef _WIN32
#define USAGE     "no serial device specified (e.g. COM1)\nusage: 'csertest <comport> [<baudrate>]'"
#define FORMAT    "\\\\.\\%s"
#else
#define USAGE     "no serial device specified (e.g. /dev/ttyS1)\nusage: 'csertest <comport> [<baudrate>]'"
#define FORMAT    "%s"
#endif


/**
 *  Server socket test program
 */
int main(int argc, char *argv[])
{
    int res, err;
    CSER_FD fd;
    cser_cfg_t cfg;
    char dev[1024];
    char r[4096];
    char s[4096];

    cfg.baudrate = CSER_BAUDRATE_9600;
    cfg.databits = CSER_DATABITS_8;
    cfg.parity   = CSER_PARITY_NONE;
    cfg.stopbits = CSER_STOPBITS_1;
    cfg.handshake= CSER_HANDSHAKE_NONE;

    if (argc == 1) {
        printf(USAGE);
        exit(EXIT_FAILURE);
    }
    snprintf(dev, sizeof(dev)-1, FORMAT, argv[1]);
    
    if (argc > 2) {
        cfg.baudrate = atoi(argv[2]);
    }

    printf("serial echo tester started.\n");
    printf("    device  : '%s'\n", dev);
    printf("    baudrate: '%i'\n", cfg.baudrate);

    err = cser_open(dev, &fd);
    if (err) {
        printf("err(%i): failed to open device '%s' \n", err, dev);
        exit(EXIT_FAILURE);
    }
    err = cser_cfg_set(fd, &cfg);
    if (err) {
        printf("err(%i): failed to configure device '%s' \n", err, dev);
        cser_close(fd);
        exit(EXIT_FAILURE);
    }

    while (1) {
        printf("your message: ");    
        fgets(s, sizeof(s)-1, stdin);
        if (strlen(s)==0 || strncmp(s, "quit", 4)==0) {
            break;
        }
        res = cser_write(fd, s, strlen(s)+1);
        if (res <= 0) {
            printf("res(%i): failed to write to device '%s' \n", res, dev);
            break;
        }

        memset(r, 0, sizeof(r));
        res = cser_read(fd, r, sizeof(r)-1);
        if (res < 0) {
            printf("res(%i): failed to read from device '%s' \n", res, dev);
            break;
        }
        if (res > 0) {
            printf("received: %s\n", r);        
            if (strncmp(r, "quit", 4) == 0) {
                break;
            }
        }
    }

    printf("bye\n");
    err = cser_close(fd);
    if (err) {
        printf("err(%i): failed to close device '%s' \n", err, dev);
        exit(EXIT_FAILURE);
    }
    return EXIT_SUCCESS;
}


