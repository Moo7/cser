/**
 * Contains portable functions for serial communication
 */

#ifndef _CSER_H
#define _CSER_H

#include <stdlib.h>
#include <stdio.h>

#ifndef CSER_VERSION
#define CSER_VERSION "0.0.2"
#endif


#ifdef __linux__
#include <termios.h>
#elif _WIN32
#include <windows.h>
#endif


#ifdef _WIN32
 #define CSER_FD HANDLE
#elif __linux__
 #define CSER_FD int
#else
 #error CSER: platform not supported!
#endif


#ifdef _WIN32
#define B50         50              // not defined in winbase.h
#define B75         75              // not defined in winbase.h
#define B110        CBR_110
#define B134        134             // not defined in winbase.h
#define B150        150             // not defined in winbase.h
#define B200        200             // not defined in winbase.h
#define B300        CBR_300
#define B600        CBR_600
#define B1200       CBR_1200
#define B1800       1800            // not defined in winbase.h
#define B2400       CBR_2400
#define B4800       CBR_4800
#define B9600       CBR_9600
#define B19200      CBR_19200
#define B38400      CBR_38400
#define B57600      CBR_57600
#define B115200     CBR_115200
#endif


/** definitions of supported physical standards */
typedef enum {
    CSER_STANDARD_RS232 = 0,
    CSER_STANDARD_RS485 = 1,
} cser_standard_t;


/** definitions of supported baud rates */
typedef enum {
    CSER_BAUDRATE_50     = B50,
    CSER_BAUDRATE_75     = B75,
    CSER_BAUDRATE_110    = B110,
    CSER_BAUDRATE_134    = B134,
    CSER_BAUDRATE_150    = B150,
    CSER_BAUDRATE_200    = B200,
    CSER_BAUDRATE_300    = B300,
    CSER_BAUDRATE_600    = B600,
    CSER_BAUDRATE_1200   = B1200,
    CSER_BAUDRATE_1800   = B1800,
    CSER_BAUDRATE_2400   = B2400,
    CSER_BAUDRATE_4800   = B4800,
    CSER_BAUDRATE_9600   = B9600,
    CSER_BAUDRATE_19200  = B19200,
    CSER_BAUDRATE_38400  = B38400,
    CSER_BAUDRATE_57600  = B57600,
    CSER_BAUDRATE_115200 = B115200,
} cser_baudrate_t;


/** definition of serial connection types */
typedef enum {
    CSER_TYPE_NORMAL               = 0,        // RS2323 and RS485
    CSER_TYPE_MULTIDROP            = 1,        // RS485 only
    CSER_TYPE_MULTIDROP_HALFDUPLEX = 2,        // RS485 only
} cser_type_t;


/** defines flow control settings */
typedef enum {
    CSER_HANDSHAKE_NONE     = 0,        // no flow control
    CSER_HANDSHAKE_HARDWARE = 1,        // flow control using additional CTS/RTS lines
    CSER_HANDSHAKE_XON_XOFF = 2,        // flow control using special XON-XOFF characters
} cser_handshake_t;


/** defines the number of data bits, remark only 7 or 8 can be configured as user */
typedef enum {
    CSER_DATABITS_5 = 5,
    CSER_DATABITS_6 = 6,
    CSER_DATABITS_7 = 7,
    CSER_DATABITS_8 = 8,
} cser_databits_t;


/** defines the number of stop bits */
typedef enum {
    CSER_STOPBITS_UNDEFINED = -1,
    CSER_STOPBITS_1         = 1,
    CSER_STOPBITS_2         = 2,
} cser_stopbits_t;


/** defines the parity settings */
typedef enum {
    CSER_PARITY_UNDEFINED = -1,
    CSER_PARITY_NONE      = 0,
    CSER_PARITY_ODD       = 1,
    CSER_PARITY_EVEN      = 2,
} cser_parity_t;


/** definition of serial line configuration settings */
typedef struct {
    cser_baudrate_t      baudrate;
    cser_type_t          type;
    cser_handshake_t     handshake;
    cser_databits_t      databits;
    cser_stopbits_t      stopbits;
    cser_parity_t        parity;
} cser_cfg_t;


/**
 * Returns the library version
 *
 * @return library version string ("MAJOR.MINOR.PATCH")
 */
const char* cser_version();


/**
 *  Opens a serial port for communication
 *
 *  @param       device    serial device name
 *  @param[out]  fd        device file descriptor
 *  @return                returns zero on success or ERRNO in case of failure
 */
int cser_open(const char* device, CSER_FD* fd);


/**
 *  Terminates serial communication with a port instance
 *
 *  @param  fd      serial port file descriptor
 *  @return         returns zero on success or ERRNO in case of failure
 */
int cser_close(CSER_FD fd);


/**
 *  Reads n-bytes from a serial port
 *
 *  @param  fd      serial port file descriptor
 *  @param  buf     destination buffer for storing data
 *  @param  size    maximum number of bytes to read from the serial port
 *  @return         returns the actual number of bytes read
 */
int cser_read(CSER_FD fd, void* buf, size_t size);


/**
 *  Writes n-bytes to a serial port
 *
 *  @param  fd      serial port file descriptor
 *  @param  buf     source buffer containing data to be written to the port
 *  @param  size    maximum number of bytes to read from the serial port
 *  @return         returns the actual number bytes written
 */
int cser_write(CSER_FD fd, void* buf, size_t size);


/**
 *  Get configuration setting for the requested serial port
 *
 *  @param  fd      serial port file descriptor
 *  @param  cfg     configuration buffer for the serial port
 *  @return         returns zero on success or ERRNO in case of failure
 */
int cser_cfg_get(CSER_FD fd, cser_cfg_t* cfg);


/**
 *  Set configuration setting for the requested serial port
 *
 *  @param  fd      serial port handle
 *  @param  cfg     configuration containing data for the port
 *  @return         returns zero on success or ERRNO in case of failure
 */
int cser_cfg_set(CSER_FD fd, cser_cfg_t* cfg);


#endif // _CSER_H


