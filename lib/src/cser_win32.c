#ifdef _WIN32

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <cser.h>
#include <_cser.h>


/**
 *  Get configuration setting for the requested serial port on
 *  platforms using MS-WINDOWS as operating system.
 *
 *  @param  fd      device file descriptor
 *  @param  cfg     configuration buffer for the serial port
 *  @return         returns zero on success, or ERRNO in case of failure
 */
int _cfg_get(CSER_FD fd, cser_cfg_t* cfg)
{
    DCB dcb;

    if ( !GetCommState(fd, &dcb) ) {
        return errno;
    }

    cfg->baudrate = dcb.BaudRate;
    cfg->databits = dcb.ByteSize;

    cfg->parity = CSER_PARITY_UNDEFINED;
    if (dcb.Parity == NOPARITY) {
        cfg->parity = CSER_PARITY_NONE;
    }
    else if (dcb.Parity == ODDPARITY) {
        cfg->parity = CSER_PARITY_ODD;
    }
    else if (dcb.Parity == EVENPARITY) {
        cfg->parity = CSER_PARITY_EVEN;
    }

    cfg->stopbits = CSER_STOPBITS_UNDEFINED;
    if (dcb.StopBits == ONESTOPBIT) {
        cfg->stopbits = CSER_STOPBITS_1;
    }
    else if (dcb.StopBits == TWOSTOPBITS) {
        cfg->stopbits = CSER_STOPBITS_2;
    }

    if (dcb.fOutxCtsFlow == TRUE && dcb.fRtsControl == RTS_CONTROL_HANDSHAKE) {
        cfg->handshake = CSER_HANDSHAKE_HARDWARE;
    }
    else if (dcb.fInX==TRUE && dcb.fOutX==TRUE) {
        cfg->handshake = CSER_HANDSHAKE_XON_XOFF;
    }
    else {
        cfg->handshake = CSER_HANDSHAKE_NONE;
    }
    return 0;
}


/**
 *  Set configuration setting for the requested serial port on platforms
 *  using MS-WINDOWS as operating system.
 *
 *  @param  fd     device file descriptor
 *  @param  cfg    configuration containing data for the port
 *  @return        returns zero on success, or ERRNO in case of failure
 */
int _cfg_set(CSER_FD fd, cser_cfg_t* cfg)
{
    DCB dcb;

    if ( !GetCommState(fd, &dcb) ) {
        return errno;
    }

    // TODO: overwrite or leave at default?
    dcb.DCBlength = sizeof(dcb);
    dcb.fBinary = TRUE;
    dcb.fOutxDsrFlow = FALSE;
    dcb.fDtrControl = FALSE;
    dcb.fDsrSensitivity = FALSE;
    dcb.fTXContinueOnXoff = FALSE;
    dcb.fErrorChar = FALSE;
    dcb.fNull = FALSE;
    dcb.fAbortOnError = FALSE;

    dcb.BaudRate = cfg->baudrate;
    dcb.ByteSize = cfg->databits;

    if (cfg->parity == CSER_PARITY_NONE) {
        dcb.fParity = FALSE;
        dcb.Parity = NOPARITY;
    }
    else {
        dcb.fParity = TRUE;
        if (cfg->parity == CSER_PARITY_ODD) {
            dcb.Parity = ODDPARITY;
        }
        else {
            dcb.Parity = EVENPARITY;
        }
    }

    if (cfg->stopbits == CSER_STOPBITS_1) {
        dcb.StopBits = ONESTOPBIT;
    }
    else {
        dcb.StopBits = TWOSTOPBITS;
    }

    dcb.fOutxCtsFlow = FALSE;
    dcb.fRtsControl = RTS_CONTROL_DISABLE;
    dcb.fOutX = FALSE;
    dcb.fInX = FALSE;

    if (cfg->handshake==CSER_HANDSHAKE_HARDWARE) {
        dcb.fOutxCtsFlow = TRUE;
        dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
    }
    else if (cfg->handshake==CSER_HANDSHAKE_XON_XOFF) {
        dcb.fOutX = TRUE;
        dcb.fInX = TRUE;
        // TODO: set buffer tresholds when using XON/XOFF?
        // dcb.XonLim = 0;
        // dcb.XoffLim = 0;
    }

    if ( !SetCommState(fd, &dcb) ) {
        return errno;
    }
    return 0;
}


/**
 *  Open a serial port for communication on platforms using MS-WINDOWS as
 *  operating system.
 *
 *  @param       serial  serial port instance
 *  @param[out]  fd      device file descriptor
 *  @return              returns zero on success, or ERRNO in case of failure
 */
int _open(const char* device, CSER_FD* fd)
{
    COMMTIMEOUTS cto;

    *fd = CreateFileA(device,
                     GENERIC_READ|GENERIC_WRITE,
                     0,     // no share
                     NULL,  // no security
                     OPEN_EXISTING,
                     FILE_ATTRIBUTE_NORMAL,
                     NULL); // no templates

    if(*fd == INVALID_HANDLE_VALUE) {
        return errno;
    }

    // set serial port to non-blocking
    memset(&cto, 0, sizeof(cto));
    cto.ReadIntervalTimeout = MAXDWORD;
    cto.ReadTotalTimeoutMultiplier = 0;
    cto.ReadTotalTimeoutConstant = 0;
    if ( !SetCommTimeouts(*fd, &cto) ) {
        return errno;
    }
    return 0;
}


/**
 *  Terminates serial communication with a port instance on platforms using
 *  MS-WINDOWS as operating system.
 *
 *  @param    fd    device file descriptor
 *  @return         returns zero on success or ERRNO in case of failure
 */
int _close(CSER_FD fd)
{
    if ( !CloseHandle(fd) ) return errno;
    return 0;
}


/**
 *  Reads n-bytes from a serial port on platforms using MS-WINDOWS as operating
 *  system.
 *
 *  @param  fd      device file descriptor
 *  @param  buf     destination buffer for storing data
 *  @param  size    maximum number of bytes to read from the serial port
 *  @return         returns the actual number of bytes read
 */
int _read(CSER_FD fd, void* buf, size_t size)
{
    DWORD count=0;
    if ( !ReadFile(fd, buf, size, &count, NULL) ) return 0;
    return (int)count;
}


/**
 *  Writes n-bytes to a serial port on platforms using MS-WINDOWS as operating
 *  system.
 *
 *  @param  fd      device file descriptor
 *  @param  buf     source buffer containing data to be written to the port
 *  @param  size    maximum number of bytes to read from the serial port
 *  @return         returns the actual number bytes written
 */
int _write(CSER_FD fd, void* buf, size_t size)
{
    DWORD count=0;
    if ( !WriteFile(fd, buf, size, &count, NULL) ) return 0;
    return (int)count;
}


#endif // _WIN32


