#ifdef __linux__

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <errno.h>
#include <cser.h>
#include <_cser.h>


/**
 *  Get configuration setting for the requested serial port on
 *  platforms using LINUX as operating system.
 *
 *  @param  fd      device file descriptor
 *  @param  cfg     configuration buffer for the serial port
 *  @return         returns zero on success, or -1 in case of failure
 */
int _cfg_get(CSER_FD fd, cser_cfg_t* cfg)
{
    int res;
    struct termios term;

    memset(&term, 0, sizeof(term));

    res = tcgetattr(fd, &term);
    if (res < 0) {
        return res;
    }

    cfg->baudrate = term.c_cflag & CBAUD;

    if (term.c_cflag & CS8) {
        cfg->databits = CSER_DATABITS_8;
    }
    else if (term.c_cflag & CS7) {
        cfg->databits = CSER_DATABITS_7;
    }
    else if (term.c_cflag & CS6) {
        cfg->databits = CSER_DATABITS_6;
    }
    else {
        cfg->databits = CSER_DATABITS_5;
    }

    if (term.c_cflag & PARENB) {
        if (term.c_cflag & PARODD) {
            cfg->parity = CSER_PARITY_ODD;
        }
        else {
            cfg->parity = CSER_PARITY_EVEN;
        }
    }
    else {
        cfg->parity = CSER_PARITY_NONE;
    }

    if (term.c_cflag & CRTSCTS) {
        cfg->handshake = CSER_HANDSHAKE_HARDWARE;
    }
    else if (term.c_iflag & (IXON+IXOFF) ) {
        cfg->handshake = CSER_HANDSHAKE_XON_XOFF;
    }
    else {
        cfg->handshake = CSER_HANDSHAKE_NONE;
    }
    return 0;
}


/**
 *  Set configuration setting for the requested serial port
 *  on platforms using LINUX as operating system.
 *
 *  @param  fd      device file descriptor
 *  @param  cfg     configuration containing data for the port
 *  @return         returns zero on success, or -1 in case of failure
 */
int _cfg_set(CSER_FD fd, cser_cfg_t* cfg)
{
    int databits = CS8;
    int parity = 0;
    int stopbits = 0;
    int handshake = 0;
    struct termios term;

    memset(&cfg, 0, sizeof(cfg));

    if (cfg->databits == CSER_DATABITS_7) {
        databits = CS7;
    }

    if (cfg->parity != CSER_PARITY_NONE) {
        parity = PARENB;
    }
    if (cfg->parity == CSER_PARITY_ODD) {
        parity |= PARODD;
    }

    if (cfg->stopbits==CSER_STOPBITS_2) {
        stopbits = CSTOPB;
    }

    if (cfg->handshake == CSER_HANDSHAKE_HARDWARE) {
        handshake = CRTSCTS;
    }
    term.c_cflag = cfg->baudrate | databits | parity | stopbits | handshake | CLOCAL | CREAD;
    term.c_iflag = IGNPAR; // TODO: ignore frame errors??

    if (cfg->handshake == CSER_HANDSHAKE_XON_XOFF) {
        term.c_iflag |= IXON+IXOFF;
    }
    return tcsetattr(fd, TCSANOW, &term);
}


/**
 *  Open a serial port for communication on platforms using LINUX
 *  as operating system.
 *
 *  @param       device  device name to be opened
 *  @param[out]  fd      device file descriptor
 *  @return              returns zero on success or ERRNO in case of failure
 */
int _open(const char* device, CSER_FD* fd)
{
    *fd = open(device, O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);
    if (*fd < 0) {
        return errno;
    }
    return 0;
}


/**
 *  Terminates serial communication with a port instance on platforms
 *  using LINUX as operating system.
 *
 *  @param  fd      device file descriptor
 *  @return         returns zero on success or ERRNO in case of failure
 */
int _close(CSER_FD fd)
{
    return close(fd);
}


/**
 *  Reads n-bytes from a serial port on platforms using LINUX as operating
 *  system.
 *
 *  @param  fd      device file descriptor
 *  @param  buf     destination buffer for storing data
 *  @param  size    maximum number of bytes to read from the serial port
 *  @return         returns the actual number of bytes read
 */
int _read(CSER_FD fd, void* buf, size_t size)
{
    return read(fd, buf, size);
}


/**
 *  Writes n-bytes to a serial port on platforms using LINUX as operating 
 *  system.
 *
 *  @param  fd      device file descriptor
 *  @param  buf     source buffer containing data to be written to the port
 *  @param  size    maximum number of bytes to read from the serial port
 *  @return         returns the actual number of bytes written
 */
int _write(CSER_FD fd, void* buf, size_t size)
{
    return write(fd, buf, size);
}


#endif // __linux__


