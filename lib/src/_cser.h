#ifndef __CSER_H_
#define __CSER_H_

#include <cser.h>

int _open(const char* device, CSER_FD* fd);
int _close(CSER_FD fd);
int _read(CSER_FD fd, void* buf, size_t size);
int _write(CSER_FD fd, void* buf, size_t size);

int _cfg_get(CSER_FD fd, cser_cfg_t* cfg);
int _cfg_set(CSER_FD fd, cser_cfg_t* cfg);

#endif // __CSER_H_


