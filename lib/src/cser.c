#include <string.h>
#include <errno.h>
#include <cser.h>
#include <_cser.h>


/**
 * Returns the library version
 *
 * @return library version string ("MAJOR.MINOR.PATCH")
 */
const char* cser_version()
{
    return CSER_VERSION;
}


/**
 *  Opens a serial port for communication
 *
 *  @param       device    serial device name
 *  @param[out]  fd        device file descriptor
 *  @return                returns zero on success or ERRNO in case of failure
 */
int cser_open(const char* device, CSER_FD* fd)
{
    if (!device || strlen(device) == 0 || !fd) {
        return EINVAL;
    }
    return _open(device, fd);
}


/**
 *  Terminates serial communication with a port instance
 *
 *  @param  fd      serial port file descriptor
 *  @return         returns zero on success or ERRNO in case of failure
 */
int cser_close(CSER_FD fd)
{
    return _close(fd);
}


/**
 *  Reads n-bytes from a serial port
 *
 *  @param  fd      serial port file descriptor
 *  @param  buf     destination buffer for storing data
 *  @param  size    maximum number of bytes to read from the serial port
 *  @return         returns the actual number of bytes read
 */
int cser_read(CSER_FD fd, void* buf, size_t size)
{
    if (!buf || size == 0) {
        return 0;
    }
    return _read(fd, buf, size);
}


/**
 *  Writes n-bytes to a serial port
 *
 *  @param  fd      serial port file descriptor
 *  @param  buf     source buffer containing data to be written to the port
 *  @param  size    maximum number of bytes to read from the serial port
 *  @return         returns the actual number bytes written
 */
int cser_write(CSER_FD fd, void* buf, size_t size)
{
    if (!buf || size == 0) {
        return 0;
    }
    return _write(fd, buf, size);
}


/**
 *  Get configuration setting for the requested serial port
 *
 *  @param  fd      serial port file descriptor
 *  @param  cfg     configuration buffer for the serial port
 *  @return         returns zero on success or ERRNO in case of failure
 */
int cser_cfg_get(CSER_FD fd, cser_cfg_t* cfg)
{
    if (!cfg) {
        return EINVAL;
    }
    return _cfg_get(fd, cfg);
}


/**
 *  Set configuration setting for the requested serial port
 *
 *  @param  fd      serial port handle
 *  @param  cfg     configuration containing data for the port
 *  @return         returns zero on success or ERRNO in case of failure
 */
int cser_cfg_set(CSER_FD fd, cser_cfg_t* cfg)
{
    if (!cfg) {
        return EINVAL;
    }
    return _cfg_set(fd, cfg);
}


