Summary
-------
A portable library for communication using serial ports.


Description
-----------
*cser*, pronounced as *Caesar* ;-), is a library providing an abstraction layer
for (RS232/RS485) serial communication on linux and windows platforms.


Installation
------------
*cser* can be build and installed using the waf_ meta build system and the 
waftools_ package. Both can be installed from the python package server PyPi_ 
as shown in the command line example below, which install both waf_ build 
system itself as well as the waftools_ package::

	pip install waftools --user
	wafinstall

Once installed the *cser* package can be build and installed as shown in the 
example below::

	waf configure --debug --prefix=~/.local
	waf build install

.. note::
	the example presented above will build and install *cser* in **~/.local/bin**
	using debugging information.


Support
-------
Defects and/or feature requests can be reported at::
	https://bitbucket.org/Moo7/cser/issues


.. _waf: https://code.google.com/p/waf/
.. _waftools: https://pypi.python.org/pypi?:action=display&name=waftools
.. _PyPi: https://pypi.python.org/pypi

