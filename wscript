#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# Michel Mooij, michel.mooij7@gmail.com

import waftools
from waftools import ccenv

top = '.'
out = 'build'
prefix = 'out'

VERSION = '0.0.2'
APPNAME = 'cser'
TREES = ['ext', 'examples', 'lib', 'test']


def options(opt):
	opt.add_option('--prefix', dest='prefix', default=prefix, help='installation prefix [default: %r]' % prefix)
	opt.load('ccenv', tooldir=waftools.location)


def configure(conf):
	conf.load('ccenv')
	conf.recurse(dirs=TREES, name='configure', mandatory=False)
	if VERSION != conf.env.CSER_VERSION:
		conf.fatal('Version mismatch: CSER_VERSION=%s VERSION=%s' % (conf.env.CSER_VERSION, VERSION))


def build(bld):
	ccenv.build(bld, trees=TREES)


def version(bld):
	print(VERSION)


def dist(ctx):
	ctx.excl = '.git/** **/.gitignore **/*~ .lock-w* .settings/** %s-*.bz2 %s/** %s/**' % (APPNAME, out, prefix)


for var in ccenv.variants():
	for ctx in ccenv.contexts():
		name = ctx.__name__.replace('Context','').lower()
		class _t(ctx):
			__doc__ = "%ss '%s'" % (name, var)
			cmd = name + '_' + var
			variant = var

