#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# Michel Mooij, michel.mooij7@gmail.com


'''
Description
-----------
Creates a new release::

	- create source distribution
	- tags the new release using git
	- [MANUAL] upload distribution to bitbucket
'''

import os
import sys
import subprocess

# WAF: distclean, configure, dist
subprocess.call('waf distclean configure dist'.split())

# WAF: get version
out = subprocess.check_output('waf version'.split())
version = out.splitlines()[0]

# GIT: tag the new release
subprocess.call('git tag -a v{0} -m "v{0}"'.format(version).split())
subprocess.call('git push origin --tags'.split())


